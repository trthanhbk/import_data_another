
<?php
include 'config.php';
/*==============================================*/
 	function _DB($sql){
        include 'config.php';
        $username           =   $data_config['database_use'] ;
        $servername         =   $data_config['database_link'];
        $database_name      =   $data_config['database_name'];
        $password           =   $data_config['database_password'];

        try {
           $conn = new PDO("mysql:host=$servername;dbname=$database_name", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            return $conn->exec($sql);
        }
        catch(PDOException $e){
            //echo "Connection failed: " . $e->getMessage();
            return false;
        }
       $conn = null;
    }
/*==============================================*/
    function _DB2(){
        include 'config.php';
        $username           =   $data_config['database_use'] ;
        $servername         =   $data_config['database_link'];
        $database_name      =   $data_config['database_name'];
        $password           =   $data_config['database_password'];

        try {
            $conn = new PDO("mysql:host=$servername;dbname=$database_name", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
           $sql = "DESCRIBE `kkm_new`";
            $stm = $conn->prepare($sql);
            $stm->execute();
            $result = $stm->setFetchMode(PDO::FETCH_ASSOC);
            $result=$stm->fetchAll();
            return $result;
        }
        catch(PDOException $e){
            echo "Connection failed:" . $e->getMessage();
            //return 'Warning';
        }
       $conn = null;
       die();
    }
 /*===========================================*/
 	function _import_to_row($colums,$data_of_row){
 		$sql = "INSERT INTO `kkm_new` ($colums) VALUES($data_of_row);";
 		$result =_DB($sql);
 		if ( $result== false){
 			return $sql;
 		}elseif(preg_match('/Warning/',$result) == 1) {
 			return 'wanning';
 		}
 		return true;		
 	}

/*============================================*/
	function _get_colums_name_in_database(){
		$colums_in_database = _DB2();

		$result = array();
	    foreach ($colums_in_database as  $value) {
	    	array_push( $result, $value['Field']);
	    }
	    return $result;
	}

/*=============================================*/
	function _convert_columns_csv($csvColumns){
		$result = array();
	    foreach ($csvColumns as  $value) {

	    	$value  = mysql_real_escape_string($value);
	    	
	    	$value  = mb_convert_encoding($value, "UTF-8", "Shift-JIS");	    	
	    	
	    		if($value == '緯度'){
	    		$value = 'longitude';
		    	}elseif ($value == '経度') {
		    		$value = 'latitude';
		    	}elseif ($value == 'ID') {
		    		$value = 'business_id';
		    	}
	    		array_push( $result, $value);
	    	       
	    }
	    return $result;
	}

/*=============================================*/
	function _convert_JIS($data){
		$result  = mysql_real_escape_string($data);	
		if (empty($result)){
			$result = 'null';
			return $result;
		}if(!isset($data) && $data){
			$result = 'null';
			return $result;
		}
		$result  = mb_convert_encoding($result, "UTF-8", "Shift-JIS");
		$result = "'".$result."'";
	    return $result;

	}
/*=============================================*/




ini_set('auto_detect_line_endings',TRUE);

$dbColumns = _get_colums_name_in_database();

$_FILES                     	= fopen($path,"r");
$csvColumns                     = fgetcsv($_FILES);
fclose($_FILES);
$log			 				= "log_".date("Y-m-d_H-i",time()).".txt";
$logfile	                    = fopen($log,'w');
$csvColumns = _convert_columns_csv($csvColumns);

$commonColumns = array();
$index = 0;
foreach($csvColumns as $col) {
    if (in_array($col, $dbColumns)){
        array_push($commonColumns, array($col => $index));
    }
    $index++;
}

$_FILES                     = fopen($path,"r");
$i = 0;
$j = 0;
$x = 0;
$error=0;
while(!feof($_FILES)){
    $row = fgetcsv($_FILES);
    if ($i != 0) {

        $dataColumns = array();
        $fields = array();
        $values = array();
        $row_error = array();
        $latitude = 0;
        $longitude = 0;
        foreach ($commonColumns as $column) {            
            foreach ($column as $field => $csvIndex) {      
                if ($field == "longitude") { $longitude = isset($row[$csvIndex]) && $row[$csvIndex] != "" ? $row[$csvIndex] : 0;}
                if ($field == "latitude") { $latitude = isset($row[$csvIndex]) && $row[$csvIndex] != ""? $row[$csvIndex] : 0; }
                array_push($fields, $field) ;
                $value =_convert_JIS(isset($row[$csvIndex]) ? $row[$csvIndex] : "");
                array_push($values, $value) ;
            }
        }
        
        array_push($fields, 'geo') ;       
        $fields = implode($fields, ",");
        array_push($values, "GeomFromText('POINT($latitude $longitude)')"); 
        $values   = implode($values, ",");
        $import   = _import_to_row($fields, $values);
        if ($import==true){
        	$x++;
        	echo "import success row ".$i ."\n\n";
        }elseif ($import === 'wanning') {
        	$error++;
        	$txt = "wanning in row ".$i."in csv file \n Field : ".$fields."\n\n Values: ".$values."\n\n SQL : ".$import."\n\n=================================================================================================";
        	fwrite($logfile, $txt);die();
        }else {        	
        	$j++;
        	$txt = "error row ".$i."in csv file \n Field : ".$fields."\n\n Values: ".$values."\n\n SQL : ".$import."\n\n=================================================================================================";
        	fwrite($logfile, $txt);
        }
        time_nanosleep(0, 50000);      
        
    }
    $i++;
}
echo"================================\n\n";
echo "success ".$x." row \n\n";
echo "error ".$j." row\n\n";
echo "wanning ".$j." row\n\n";

echo"================================\n\n";
 echo $i;
fclose($_FILES);



